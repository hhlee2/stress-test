# stress-test

## How To Use

```
usage: stress-test.py [-h] --url URL [-c C] [-n N]
[--show-response SHOW_RESPONSE] [-f F]

optional arguments:
-h, --help show this help message and exit
--url URL [str] 요청을 보낼 URL
-c C [int] 동시에 요청하는 요청수. 기본적으로 한번에 한 요청만을 보낸다.
-n N [int] 성능을 검사하기위해 보내는 요청수. 기본값으로 요청을 한번만 보낸다.
--show-response SHOW_RESPONSE [bool] 요청에 대한 응답을 표시할지 여부를 결정한다.
-f F [*.json] 요청에 사용할 Body Data
```

