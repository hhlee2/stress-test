import json
import time
import asyncio
import aiohttp
import argparse



data = []

HTML_STATUSES = {x for x in range(100, 600)}
HTML_STATUSES.remove(200)
HTML_STATUSES.remove(429)


async def getResponse(url,requests_body,showResponse):
    async with aiohttp.ClientSession() as client:
        async with client.post(url=url, json=requests_body) as response:
            #print(response)
            result = await response.text()
            if showResponse:
                print(result)
            datum = json.loads(result)
            data.append(datum)

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--url', type=str, help='[str] 요청을 보낼 URL', required=True)
    parser.add_argument('-c', type=int, default=1, help='[int] 동시에 요청하는 요청수. 기본적으로 한번에 한 요청만을 보낸다.')
    parser.add_argument('-n', type=int, default=1, help='[int] 성능을 검사하기위해 보내는 요청수. 기본값으로 요청을 한번만 보낸다.')
    parser.add_argument('--show-response', type=bool, default=False, help="[bool] 요청에 대한 응답을 표시할지 여부를 결정한다.")
    parser.add_argument('-f',type=argparse.FileType('rt', encoding='UTF-8'), help="[*.json] 요청에 사용할 Body Data")
    args = parser.parse_args()

    showResponse = args.show_response
    url = args.url
    n = args.n
    concurently = args.c
    showResponse = args.show_response
    totalTime = 0
    num = 0
    requests_body = json.load(args.f)

    loop = asyncio.get_event_loop()

    for tasks in range(n):
        start = time.time()
        loop.run_until_complete(
            asyncio.gather(*(getResponse(url,requests_body, showResponse) for i in range(concurently)))
        )
        end = time.time()
        totalTime += end - start
        print(f"- iter {num+1} (Send {concurently} requests concurently)")
        print(f"  Response Times : {end - start}")
        num += 1
    print("-"*10)
    print(f"Total Response Times : {totalTime}")
    print(f"Average Time : {totalTime / num}")

